﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour {

    private static Main instance;
    public static Main Instance { get { return instance; } }

    //Singleton
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
            Debug.Log("Another Main.cs attempted to be loaded and was destroyed.");
        }
        else
        {
            instance = this;
        }
    }

    private void Start() {

        InitializeScreens();
        SetupCameraRig();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            DebugGUI.Instance.ShowGUI();
        }
    }

    #region CameraRig
    public CameraRig cameraRigScript;

    void SetupCameraRig() {
        cameraRigScript = FindObjectOfType<CameraRig>();
    }

    #endregion

    #region Screen Management
    //public Camera[] cameras = new Camera[6];

    //public RectTransform[] screenRectTransforms = new RectTransform[6];

    private void InitializeScreens() {

        ////Set Resolution - top row 1920x1080 bottom row 1920x1920
        //Vector2Int appResolution = new Vector2Int(5760, 3000);
        //Screen.SetResolution((int)appResolution.x, (int)appResolution.y, false);

        ////Set rect values for screen canvas panels, panel are top right orientated.
        //Rect[] screenRects = new Rect[] {
        //    new Rect(0, -750, 1920, 750),
        //    new Rect(1920, -750, 1920, 750),
        //    new Rect(3840, -750, 1920, 750),
        //    new Rect(0, -1830, 1920, 750),
        //    new Rect(1920, -2280, 1920, 1200),
        //    new Rect(3840, -2280, 1920, 1200)
        //};
        //for (int i = 0; i < screenRects.Length; i++)
        //{
        //    screenRectTransforms[i].anchoredPosition = new Vector2(screenRects[i].x, screenRects[i].y);
        //    screenRectTransforms[i].sizeDelta = new Vector2(screenRects[i].width, screenRects[i].height);
        //}

        ////Get normal values for camera rects.
        //float wallHeight = Mathf.InverseLerp(0, Screen.height, 750);
        //float wallWidth = Mathf.InverseLerp(0, Screen.width, 1920);
        //float floorWidth = Mathf.InverseLerp(0, Screen.width, 1920);
        //float floorHeight = Mathf.InverseLerp(0, Screen.height, 1200);

        //Rect[] cameraRects = new Rect[] {
        //    new Rect(0, 1 - wallHeight, wallWidth, wallHeight),
        //    new Rect(wallWidth, 1 - wallHeight, wallWidth, wallHeight),
        //    new Rect(wallWidth * 2, 1 - wallHeight, wallWidth, wallWidth),
        //    new Rect(0, floorHeight - wallHeight, wallWidth, wallHeight),
        //    new Rect(wallWidth, 0, floorWidth, floorHeight),
        //    new Rect(wallWidth * 2, 0, floorWidth, floorHeight)
        //};

        //for (int i = 0; i < cameras.Length; i++)
        //{
        //    cameras[i].rect = cameraRects[i];
        //}
    }

    #endregion

}
