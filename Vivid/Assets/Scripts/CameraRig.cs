﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class CameraRig : MonoBehaviour {

    
    float movementScalar = 0.005f;
    public float movementMultiplier = 250;
    public GameObject campsuleCamGo;
    public GameObject cameraRigGo;
    public float poleHeight = 20;
    private float poleCache;
    [Range(0,1)]
    public float rotationScalar = 0.1f;
    public float distance;
    public Vector2 pos;
    private void Start()
    {
        poleCache = poleHeight;

        foreach (Camera cam in GetComponentsInChildren<Camera>())
        {
            cam.aspect = 1;
        }
    }

    public void UpdateCameraPosition(Vector2 pos)
    {
        this.pos = pos;
    }

    // Update is called once per frame
    void Update () {

        distance = Vector3.Distance(cameraRigGo.transform.position, Vector3.zero);
        campsuleCamGo.transform.localPosition = Vector3.MoveTowards(campsuleCamGo.transform.localPosition, new Vector3(pos.x, campsuleCamGo.transform.localPosition.y, pos.y), movementScalar);
        cameraRigGo.transform.position = Vector3.MoveTowards(cameraRigGo.transform.position, new Vector3(pos.x * movementMultiplier, poleHeight, pos.y * movementMultiplier), movementScalar * movementMultiplier);
        cameraRigGo.transform.rotation = Quaternion.AngleAxis(distance * rotationScalar, new Vector3(cameraRigGo.transform.position.z, 0, cameraRigGo.transform.position.x * -1));

        if (poleCache != poleHeight)
            cameraRigGo.transform.position = new Vector3(cameraRigGo.transform.position.x, poleHeight, cameraRigGo.transform.position.z);

        
    }
}
