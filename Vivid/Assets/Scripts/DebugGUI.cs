﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugGUI : MonoBehaviour {

    private static DebugGUI instance;
    public static DebugGUI Instance { get { return instance; } }

    bool show;

    //Singleton
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
            Debug.Log("Another DebugGUI.cs attempted to be loaded and was destroyed.");
        }
        else
        {
            instance = this;
        }
    }

    public void ShowGUI() {

        if (show)
        {
            show = false;
        }
        else {
            show = true;
        }
        
    }

    public void HideGUI() {

        show = false;
    }

    public Vector2 trackPadPixelPos;
    public Vector2 trackpadPos;
    public Vector2 remapedTrackpadPos;

    void OnGUI() {

        if (!show)
            return;

        GUI.skin.box.fontSize = 42;
        GUI.skin.label.fontSize = 42;
        
        GUI.skin = Assets.Instance.debugGUISkin;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height),"");
        GUILayout.Label(string.Format("Resolution x: {0} y: {1}", Screen.width, Screen.height));


        //foreach (Camera cam in Main.Instance.cameras) {
        //    GUILayout.Label(string.Format("Camera Scaled Pixels Size: x {0} y {1}", cam.scaledPixelWidth, cam.scaledPixelHeight));
        //}

        GUI.skin.box.fontSize = 42;
        GUI.Box(new Rect(Screen.width - 1000, Screen.height -1000, 950, 950), "Camera Track Pad");
        // Mouse area        
        float remapY = Mathf.InverseLerp(0, Screen.height, Input.mousePosition.y);
        remapY = Mathf.Lerp(Screen.height, 0, remapY);
        GUI.Box(new Rect(trackpadPos.x, trackpadPos.y, 42, 42), "+");

        if (Input.GetMouseButton(0)) {
            if (Input.mousePosition.x > Screen.width - 1000 &&
                Input.mousePosition.x < Screen.width - 1000 + 950 &&
                Input.mousePosition.y > 50 &&
                Input.mousePosition.y < 1000
                )
            {
                trackpadPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                trackpadPos = new Vector2(Mathf.InverseLerp(Screen.width - 1000, Screen.width - 1000 + 950, Input.mousePosition.x), Mathf.InverseLerp(50, 1000, Input.mousePosition.y));
                GUILayout.Label(string.Format("Trackpad Hit: x: {0} y: {1}", trackpadPos.x, trackpadPos.y));
                remapedTrackpadPos = new Vector2(Mathf.Lerp(-1, 1, trackpadPos.x), Mathf.Lerp(-1, 1, trackpadPos.y));
                GUILayout.Label(string.Format("Remaped Trackpad Hit: x: {0} y: {1}", trackpadPos.x, trackpadPos.y));
                Main.Instance.cameraRigScript.UpdateCameraPosition(remapedTrackpadPos);
            }
            GUILayout.Label(string.Format("Mouse Position x: {0} y: {1}", Input.mousePosition.x, Input.mousePosition.y));
        }

    }
}
