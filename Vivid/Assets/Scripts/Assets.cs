﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Assets : MonoBehaviour {

    private static Assets instance;
    public static Assets Instance { get { return instance; } }

    public GUISkin debugGUISkin;
    
    //Singleton
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
            Debug.Log("Another Assets.cs attempted to be loaded and was destroyed.");
        }
        else
        {
            instance = this;
        }
    }
}
