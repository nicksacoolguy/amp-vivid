﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comms : MonoBehaviour {

    private static Comms instance;
    public static Comms Instance { get { return instance; } }

    //Singleton
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
            Debug.Log("Another Comms.cs attempted to be loaded and was destroyed.");
        }
        else
        {
            instance = this;
        }
    }
}
