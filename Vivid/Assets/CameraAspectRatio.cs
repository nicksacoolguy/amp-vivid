﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAspectRatio : MonoBehaviour
{

    [Header("Read")]
    public float previousAspectRatio;
    [Header("Write")]
    public float aspectRation;
    public bool set;
    public bool reset;
    
    private Camera thisCamera;
    // Use this for initialization
    void Start()
    {
        thisCamera = GetComponent<Camera>();
        previousAspectRatio = thisCamera.aspect;
    }

    private void Update()
    {
        Debug.Log(Mathf.InverseLerp(0, 1920, 1200));

        if (thisCamera == null)
            return;

        if (reset) {
            reset = false;
            previousAspectRatio = thisCamera.aspect;
            thisCamera.ResetAspect();
        }

        if (set) {
            set = false;
            previousAspectRatio = thisCamera.aspect;
            thisCamera.aspect = aspectRation;
        }
    }
}